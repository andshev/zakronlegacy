﻿using System.Drawing;
using ManagedOpenGl;
using MeteorGl.Engine;

namespace Zakron
{
    public class RocketEnemy : ZakronObject
    {
        public bool Started = false;
        public bool FlewAway = false;
        public bool Boom = false;

        readonly Enemy _enemy;
        
        public RocketEnemy(Enemy p)
        {
            _enemy = p;
            RadiusZ = 1.0f;
            Reset();
        }

        public void Reset()
        {
            Started = false;
            PosX = _enemy.PosX - _enemy.Drift;
            PosY = _enemy.PosY;
            PosZ = _enemy.PosZ;
            Compile();
        }

        public void Start()
        {
            Started = true;
        }

        public void DoBoom()
        {
            Boom = true;
            Started = false;
        }

        public void FlyAway()
        {
            Started = false;
            FlewAway = true;
        }

        public void Compile()
        {
            gl.NewList(Program.GLL_ROCKETENEMY, gl.COMPILE);

            gl.PushAttrib(gl.ALL_ATTRIB_BITS);

            gl.LineWidth(1f);
            gl.Color3ub(Color.Red.R, Color.Red.G, Color.Red.B);

            gl.Begin(gl.LINE_LOOP);
            gl.Vertex3f(0f, 0f, 0f);
            gl.Vertex3f(0.2f, 0f, -2f);
            gl.Vertex3f(-0.2f, 0f, -2f);
            gl.End();

            gl.Begin(gl.LINE_LOOP);
            gl.Vertex3f(0f, 0f, 0f);
            gl.Vertex3f(0f, 0.2f, -2f);
            gl.Vertex3f(0f, -0.2f, -2f);
            gl.End();

            gl.PopAttrib();

            gl.EndList();
        }

        public void Draw()
        {
            PosZ += 7f;

            gl.Translatef(PosX, PosY, PosZ);
            gl.Rotatef(MetEngine.Timer.GetCount() % 360, 0f, 0f, 1f);
            
            gl.CallList(Program.GLL_ROCKETENEMY);

            if (PosZ > 20)
                FlyAway();
        }

        public override bool CollidesWith(ZakronObject obj)
        {
            float w2 = obj.SizeW / 2;
            float h2 = obj.SizeD / 2;

            if (this.PosX >= obj.PosX - w2 && this.PosX <= obj.PosX + w2 && this.PosZ <= obj.PosZ + obj.SizeD && this.PosZ >= obj.PosZ - obj.SizeD)
                return true;
            else
                return false;
        }
    }
}
