﻿namespace Zakron
{
    partial class WinStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxOgranFPS = new System.Windows.Forms.CheckBox();
            this.cbxFullScreen = new System.Windows.Forms.CheckBox();
            this.lbxVideo = new System.Windows.Forms.ListBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbxOgranFPS);
            this.groupBox1.Controls.Add(this.cbxFullScreen);
            this.groupBox1.Controls.Add(this.lbxVideo);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(196, 203);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // cbxOgranFPS
            // 
            this.cbxOgranFPS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbxOgranFPS.AutoSize = true;
            this.cbxOgranFPS.Checked = true;
            this.cbxOgranFPS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxOgranFPS.Location = new System.Drawing.Point(6, 180);
            this.cbxOgranFPS.Name = "cbxOgranFPS";
            this.cbxOgranFPS.Size = new System.Drawing.Size(70, 17);
            this.cbxOgranFPS.TabIndex = 2;
            this.cbxOgranFPS.Text = "Limit FPS";
            this.cbxOgranFPS.UseVisualStyleBackColor = true;
            // 
            // cbxFullScreen
            // 
            this.cbxFullScreen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbxFullScreen.AutoSize = true;
            this.cbxFullScreen.Location = new System.Drawing.Point(6, 157);
            this.cbxFullScreen.Name = "cbxFullScreen";
            this.cbxFullScreen.Size = new System.Drawing.Size(79, 17);
            this.cbxFullScreen.TabIndex = 1;
            this.cbxFullScreen.Text = "Full Screen";
            this.cbxFullScreen.UseVisualStyleBackColor = true;
            // 
            // lbxVideo
            // 
            this.lbxVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbxVideo.FormattingEnabled = true;
            this.lbxVideo.Location = new System.Drawing.Point(6, 19);
            this.lbxVideo.Name = "lbxVideo";
            this.lbxVideo.Size = new System.Drawing.Size(184, 121);
            this.lbxVideo.TabIndex = 0;
            // 
            // btnStart
            // 
            this.btnStart.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnStart.Location = new System.Drawing.Point(28, 226);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Play";
            this.btnStart.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnExit.Location = new System.Drawing.Point(117, 226);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // WinStart
            // 
            this.AcceptButton = this.btnStart;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnExit;
            this.ClientSize = new System.Drawing.Size(220, 264);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "WinStart";
            this.Text = "Zakron";
            this.Load += new System.EventHandler(this.WinStart_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnExit;
        public System.Windows.Forms.CheckBox cbxFullScreen;
        public System.Windows.Forms.ListBox lbxVideo;
        public System.Windows.Forms.CheckBox cbxOgranFPS;
    }
}