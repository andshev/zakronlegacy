﻿using System;
using System.Drawing;

namespace Zakron
{
    public class ZakronObject
    {
        public float PosX = 0.0f;
        public float PosY = 0.0f;
        public float PosZ = 0.0f;
        public float SizeW = 0.0f;
        public float SizeH = 0.0f;
        public float SizeD = 0.0f;
        public float RadiusZ = 0.0f;
        public Color Color = Color.White;

        public void SetBounds(float width, float height, float depth)
        {
            SizeW = width;
            SizeH = height;
            SizeD = depth;
            RadiusZ = SizeW / 2;
        }

        public virtual bool CollidesWith(ZakronObject obj)
        {
            if (Math.Sqrt((obj.PosX - this.PosX) * (obj.PosX - this.PosX) + (obj.PosZ - this.PosZ) * (obj.PosZ - this.PosZ)) <= (obj.RadiusZ + this.RadiusZ))
                return true;
            else
                return false;
        }
    }
}
