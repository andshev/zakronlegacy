﻿using System;
using System.Collections.Generic;
using System.Drawing;
using LightwaveUtils;
using ManagedOpenGl;
using MeteorGl.Engine.Timers;

namespace Zakron
{
    public class Enemy : Model
    {
        public enum ManeuverType : byte
        {
            None = 0,
            LeftRight = 1
        }
        
		public float RotZ;
		public float Drift;
		public float Speed = 0.1f;
        public bool Destroyed;
        public float DSF = 1.0f;
        public bool Visible;
        public List<RocketEnemy> Rockets = new List<RocketEnemy>();
        public ManeuverType Maneuver;

        readonly Landscape _landscape;
        Random _rnd;
        int _navNext;
        int _navRandom;
        int _fireNext;
        byte _navMode;

        public Enemy(string modelname, Color c, Landscape level)
        {
            ModelName = modelname;
            _landscape = level;
            RadiusZ = 2.7f;
            SizeW = 2.6f * 2f;
            SizeH = 0.4f * 3f;
            SizeD = 2.0f * 2f;
            MeshLwo = new LWO_Model($"../Res/{ModelName}.lwo");
            MeshLwo.Load();
            Color = c;
            Compile();
            Reset(false);
        }

        public void Reset(bool changecolor)
        {
            Visible = false;
            _rnd = new Random(DateTime.Now.Millisecond);
            Speed = (float)(_rnd.NextDouble() + 3f);
            PosX = (float)_rnd.Next(-20, 20);
            PosZ = -700f;
            if (changecolor)
                Color = Color.FromArgb(_rnd.Next(int.MaxValue));
            DSF = 1.0f;
            Maneuver = ManeuverType.LeftRight;
            Destroyed = false;
            Visible = true;
        }

        public void Compile()
        {
            LWO_Layer layer0 = MeshLwo.Layers[0];
            uint pvi;

            gl.NewList(Program.GLL_MESH_ENEMY, gl.COMPILE);

            foreach (LWO_Polygon polygon in layer0.Polygons)
            {
                gl.Begin(gl.LINE_STRIP);

                for (int i = 0; i < polygon.NumVerts; i++)
                {
                    pvi = polygon.VertexIndices[i];
                    gl.Vertex3fv(layer0.Points[pvi].Coords);
                }

                gl.End();
            }

            gl.EndList();
        }

        public void Destroy()
        {
            Destroyed = true;
        }

        public void NavigateFire()
        {
            PosZ += Speed;
            
            if (MetTimer.Count() >= _navNext)
            {
                _navRandom = _rnd.Next(100);

                if (Maneuver == ManeuverType.LeftRight)
                {
                    if (_navRandom < 20)
                        _navMode = 1;
                    else if (_navRandom > 80)
                        _navMode = 2;
                    else
                        _navMode = 0;
                }

                _navNext = MetTimer.Count() + 500;
            }

            if (_navMode == 1)
                WannaLeft();

            if (_navMode == 2)
                WannaRight();

            if (Destroyed)
                return;

            if (MetTimer.Count() >= _fireNext)
            {
                _navRandom = _rnd.Next(100);

                if (_navRandom > 70)
                    Fire();

                _fireNext = MetTimer.Count() + 200;
            }
        }

        private void WannaLeft()
        {
            if (PosX - SizeW <= -_landscape.Border)
                return;

            RotZ += 3f;
            PosX -= 0.5f;
            Drift -= 0.2f;
        }

        private void WannaRight()
        {
            if (PosX + SizeW >= _landscape.Border)
                return;

            RotZ -= 3f;
            PosX += 0.5f;
            Drift += 0.2f;
        }

        private void Fire()
        {
            RocketEnemy r = new RocketEnemy(this);
            r.Start();
            Rockets.Add(r);
        }

        public void Draw()
        {
            if (Visible)
            {
                gl.PushAttrib(gl.ALL_ATTRIB_BITS);

                if (!Destroyed)
                {
                    gl.Color4ub(Color.R, Color.G, Color.B, (byte)(255 * DSF));
                }
                else
                {
                    gl.Scalef(
                        1f + (1 - DSF) * 3,
                        1f + (1 - DSF) * 3,
                        1f + (1 - DSF) * 3);
                    gl.Color4f(50f, 50f, 50f, DSF / 7f);
                }
                
                gl.LineWidth(0.2f);

                gl.CallList(Program.GLL_MESH_ENEMY);

                gl.PopAttrib();
            }
        }
   }
}
