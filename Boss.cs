﻿using System;
using System.Collections.Generic;
using System.Drawing;
using LightwaveUtils;
using ManagedOpenGl;
using MeteorGl.Engine.Timers;
using MeteorGl.Mathematics;

namespace Zakron
{
    public class BossTurrel : ZakronObject
    {
        public int Health;

        public void Damage()
        {
            if (Health > 0)
                Health--;
        }
    }

    public class Boss : Model
    {
        public bool Visible = false;
        public List<RocketBoss> Rockets = new List<RocketBoss>();
        public List<BossTurrel> Turrels = new List<BossTurrel>();
        public bool CanDamage;
        
        float RotZ;
        Random _rnd;
        int _stateX, _stateY, _stateZ;
        double _scale;

        readonly Vector3[] _navpoints = {
            new Vector3(1000f, 500f, -3000f),
            new Vector3(0f, 0f, -350f)
        };
        readonly Player _igrokPtr;
        readonly Vector3[] _startPoints = {
            new Vector3(-245, -5, 10 - 350),
            new Vector3(-195, -25, 70 - 350),
            new Vector3(-130, -45, 150 - 350),
            new Vector3(-33, -55, 232 - 350),
            new Vector3(33, -55, 232 - 350),
            new Vector3(130, -45, 150 - 350),
            new Vector3(195, -25, 70 - 350),
            new Vector3(245, -5, 10 - 350)
        };
        int _fireNextThink = 0;
        int _fireMode = 0;
        int _fireNextTurrel = 0;
        int _fireEnough = 0;

        public Boss(string modelname, Color c, Player igrokptr)
        {
            ModelName = modelname;
            Color = c;
            _igrokPtr = igrokptr;
            SizeW = 280f * 2f;
            SizeD = 19f * 2f;
            SizeH = 240f * 2f;
            _rnd = new Random();

            BossTurrel turrel;

            turrel = new BossTurrel();
            turrel.PosX = -245f;
            turrel.PosZ = _navpoints[1].Z;
            turrel.SetBounds(10f, 10f, 10f);
            Turrels.Add(turrel);

            turrel = new BossTurrel();
            turrel.PosX = -194f;
            turrel.PosZ = _navpoints[1].Z;
            turrel.SetBounds(20f, 20f, 10f);
            Turrels.Add(turrel);

            turrel = new BossTurrel();
            turrel.PosX = -120f;
            turrel.PosZ = _navpoints[1].Z;
            turrel.SetBounds(20f, 20f, 10f);
            Turrels.Add(turrel);

            turrel = new BossTurrel();
            turrel.PosX = -35f;
            turrel.PosZ = _navpoints[1].Z;
            turrel.SetBounds(30f, 30f, 10f);
            Turrels.Add(turrel);

            turrel = new BossTurrel();
            turrel.PosX = 35f;
            turrel.PosZ = _navpoints[1].Z;
            turrel.SetBounds(30f, 30f, 10f);
            Turrels.Add(turrel);

            turrel = new BossTurrel();
            turrel.PosX = 120f;
            turrel.PosZ = _navpoints[1].Z;
            turrel.SetBounds(20f, 20f, 10f);
            Turrels.Add(turrel);

            turrel = new BossTurrel();
            turrel.PosX = 194f;
            turrel.PosZ = _navpoints[1].Z;
            turrel.SetBounds(20f, 20f, 10f);
            Turrels.Add(turrel);

            turrel = new BossTurrel();
            turrel.PosX = 245f;
            turrel.PosZ = _navpoints[1].Z;
            turrel.SetBounds(10f, 10f, 10f);
            Turrels.Add(turrel);

            MeshLwo = new LWO_Model($"../Res/{ModelName}.lwo");
            MeshLwo.Load();
            Compile();
            Reset();
        }

        private void Compile()
        {
            LWO_Layer layer0 = MeshLwo.Layers[0];
            uint pvi;

            gl.NewList(Program.GLL_MESH_BOSS, gl.COMPILE);

            foreach (LWO_Polygon polygon in layer0.Polygons)
            {
                gl.Begin(gl.LINE_STRIP);

                for (int i = 0; i < polygon.NumVerts; i++)
                {
                    pvi = polygon.VertexIndices[i];
                    gl.Vertex3fv(layer0.Points[pvi].Coords);
                }

                gl.End();
            }

            gl.EndList();
        }

        public void Reset()
        {
            CanDamage = false;

            _stateX = 0;
            _stateY = 0;
            _stateZ = 0;
            _scale = 0.0;
            RotZ = 80f;
            PosX = _navpoints[0].X;
            PosY = _navpoints[0].Y;
            PosZ = _navpoints[0].Z;

            Rockets.Clear();
            foreach (BossTurrel t in Turrels)
                t.Health = 100;
        }

        public void ThinkNFire()
        {
            if (_fireMode == 0)
                Fire0();
            else if (_fireMode == 1)
                Fire1();
        }

        private void Fire0()
        {
            RocketBoss rb;
            Vector3 target;

            if (MetTimer.Count() >= _fireNextThink)
            {
                if (Turrels[_fireNextTurrel].Health > 0)
                {
                    target = new Vector3(_igrokPtr.PosX, _igrokPtr.PosY, _igrokPtr.PosZ);
                    rb = new RocketBoss(this, _startPoints[_fireNextTurrel], target);
                    rb.Start();
                    Rockets.Add(rb);
                }

                _fireNextTurrel++;
                _fireEnough++;
                if (_fireNextTurrel >= 8)
                    _fireNextTurrel = 0;
                _fireNextThink = MetTimer.Count() + 200;
                if (_fireEnough >= 16)
                {
                    _fireEnough = 0;
                    _fireMode = 1;
                }
            }
        }

        private void Fire1()
        {
            RocketBoss rb;
            Vector3 target;

            if (MetTimer.Count() >= _fireNextThink)
            {
                if (Turrels[_fireNextTurrel].Health > 0)
                {
                    target = new Vector3(_startPoints[_fireNextTurrel].X, _igrokPtr.PosY, _igrokPtr.PosZ);
                    rb = new RocketBoss(this, _startPoints[_fireNextTurrel], target);
                    rb.Start();
                    Rockets.Add(rb);
                }

                _fireNextTurrel++;
                _fireEnough++;

                if (_fireNextTurrel >= 8)
                    _fireNextTurrel = 0;

                _fireNextThink = MetTimer.Count() + 200;

                if (_fireEnough >= 32)
                {
                    _fireEnough = 0;
                    _fireMode = 0;
                }
            }
        }

        public void Draw()
        {
            if (_stateX == 0 )
            {
                if (PosX > _navpoints[1].X)
                    PosX -= 1f;
                else
                    _stateX = 1;
            }

            if (_stateY == 0)
            {
                if (PosY > _navpoints[1].Y)
                    PosY -= 0.5f;
                else
                    _stateY = 1;
            }

            if (_stateZ == 0)
            {
                if (PosZ < _navpoints[1].Z)
                    PosZ += 2.65f;
                else
                    _stateZ = 1;
            }

            if (_scale < 1.0)
                _scale += 0.001;

            if (RotZ > 0f)
                RotZ /= 1.005f;

            if (!CanDamage)
            {
                if (_stateX == 1 && _stateY == 1 && _stateZ == 1)
                    CanDamage = true;
            }
            
            gl.Translatef(PosX, PosY, PosZ);
            gl.Rotatef(RotZ, 0f, 0f, 1f);
            gl.Scaled(_scale, _scale, _scale);

            gl.PushAttrib(gl.ALL_ATTRIB_BITS);
            gl.Color3ub(Color.R, Color.G, Color.B);
            gl.LineWidth(1f);
            gl.CallList(Program.GLL_MESH_BOSS);
            gl.PopAttrib();
        }

        public bool Destroyed()
        {
            foreach (BossTurrel bt in Turrels)
            {
                if (bt.Health > 0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
