using System;
using System.Collections.Generic;
using System.Drawing;
using LightwaveUtils;
using ManagedOpenGl;
using MeteorGl.Engine.Timers;

namespace Zakron
{
    public class Player : Model
	{
		public float RotZ = 0.0f;
		public float Drift = 0.0f;
		public float Speed = 0.1f;
        public List<RocketPlayer> Rockets = new List<RocketPlayer>();
        public bool Destroyed = false;
        public float DSF = 1.0f;
        public int Scores = 0;
        public byte FireLevel = 1;
        public int FireInterval = 200;
        public int Lives = 3;

        int _fireNextThink = 0;

        public Player(string modelname, Color c)
        {
            ModelName = modelname;
            RadiusZ = 2.5f;
            PosY = 0f;
            PosZ = 0f;
            SizeW = 2.0f * 2f;
            SizeH = 0.6f * 3f;
            SizeD = 2.0f * 2f;
            Color = c;
            MeshLwo = new LWO_Model($"../Res/{ModelName}.lwo");
            MeshLwo.Load();
            Compile();
            Reset(true);
        }

        public void Reset(bool newgame)
        {
            PosX = 0f;
            DSF = 1.0f;
            Destroyed = false;

            if (newgame)
            {
                FireInterval = 200;
                FireLevel = 1;
                Scores = 0;
                Lives = 3;
            }
        }

        public void Reset()
        {
            Reset(false);
        }

        public void Compile()
        {
            LWO_Layer layer0 = MeshLwo.Layers[0];
            uint pvi;

            gl.NewList(Program.GLL_MESH_PLAYER, gl.COMPILE);

            foreach (LWO_Polygon polygon in layer0.Polygons)
            {
                gl.Begin(gl.LINE_STRIP);

                for (int i = 0; i < polygon.NumVerts; i++)
                {
                    pvi = polygon.VertexIndices[i];
                    gl.Vertex3fv(layer0.Points[pvi].Coords);
                }

                gl.End();
            }

            //for (short s = 0; s < f.f.Length; s++)
            //{
            //    gl.Begin(gl.LINE_STRIP);
            //    gl.Vertex3f(f.f[s].v[0].px, f.f[s].v[0].py, f.f[s].v[0].pz);
            //    gl.Vertex3f(f.f[s].v[1].px, f.f[s].v[1].py, f.f[s].v[1].pz);
            //    gl.Vertex3f(f.f[s].v[2].px, f.f[s].v[2].py, f.f[s].v[2].pz);
            //    gl.End();
            //}

            gl.EndList();
        }

        public void DrawStatic()
        {
            gl.PushAttrib(gl.ALL_ATTRIB_BITS);
            gl.LineWidth(1f);

            if (Destroyed)
            {
                gl.Rotatef(MetTimer.Count() % 360, 0.1f * DSF, 0.3f * DSF, 1.0f * DSF);
                gl.Color4f(1f, 1f, 1f, (float)Math.Sin(DSF));
            }
            else
                gl.Color3ub(Color.R, Color.G, Color.B);

            gl.CallList(Program.GLL_MESH_PLAYER);

            gl.PopAttrib();
        }
        
        public void Fire()
        {
            RocketPlayer r;

            if (MetTimer.Count() >= _fireNextThink)
            {
                if (FireLevel == 1)
                {
                    r = new RocketPlayer(this);
                    r.Start();
                    Rockets.Add(r);
                }

                if (FireLevel > 1)
                {
                    r = new RocketPlayer(this);
                    r.PosX -= SizeW / 2;
                    r.PosZ += 4f;
                    r.Start();
                    Rockets.Add(r);
                    r = new RocketPlayer(this);
                    r.PosX += SizeW / 2;
                    r.PosZ += 4f;
                    r.Start();
                    Rockets.Add(r);
                }

                _fireNextThink = MetTimer.Count() + FireInterval;
            }
        }

        public void Destroy()
        {
            Destroyed = true;
            Lives--;
        }
    }
}
