﻿using System.Drawing;
using ManagedOpenGl;
using MeteorGl.Engine;

namespace Zakron
{
    public class RocketPlayer : ZakronObject
    {
        public bool Started;
        public bool FlewAvay;
        public bool Boom;

        readonly Player _player;
        
        public RocketPlayer(Player player)
        {
            _player = player;
            RadiusZ = 1.0f;
            Reset();
        }

        public void Reset()
        {
            Started = false;
            PosX = _player.PosX - _player.Drift;
            PosY = _player.PosY;
            PosZ = _player.PosZ;
            Compile();
        }

        public void Start()
        {
            Started = true;
        }

        public void DoBoom()
        {
            Boom = true;
            Started = false;
        }

        public void FlyAway()
        {
            Started = false;
            FlewAvay = true;
        }

        public void Compile()
        {
            gl.NewList(Program.GLL_ROCKETPLAYER, gl.COMPILE);

            gl.PushAttrib(gl.ALL_ATTRIB_BITS);

            gl.LineWidth(1f);
            gl.Color3ub(Color.Lime.R, Color.Lime.G, Color.Lime.B);

            gl.Begin(gl.LINE_LOOP);
            gl.Vertex3f(0f, 0f, 0f);
            gl.Vertex3f(0.2f, 0f, 2f);
            gl.Vertex3f(-0.2f, 0f, 2f);
            gl.End();

            gl.Begin(gl.LINE_LOOP);
            gl.Vertex3f(0f, 0f, 0f);
            gl.Vertex3f(0f, 0.2f, 2f);
            gl.Vertex3f(0f, -0.2f, 2f);
            gl.End();

            gl.PopAttrib();

            gl.EndList();
        }

        public void Draw()
        {
            PosZ -= 4f;

            gl.Translatef(PosX, PosY, PosZ);
            gl.Rotatef(MetEngine.Timer.GetCount() % 360, 0f, 0f, 1f);

            gl.CallList(Program.GLL_ROCKETPLAYER);

            if (PosZ < -700)
                FlyAway();
        }

        public override bool CollidesWith(ZakronObject obj)
        {
            float w2 = obj.SizeW / 2;
//            float h2 = obj.SizeD / 2;

            if (PosX >= obj.PosX - w2 && PosX <= obj.PosX + w2 && PosZ <= obj.PosZ + obj.SizeD && PosZ >= obj.PosZ - obj.SizeD)
                return true;
            else
                return false;
        }
    }
}
