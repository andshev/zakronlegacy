﻿using System.Drawing;
using ManagedOpenGl;

namespace Zakron
{
    public class Landscape
    {
        public float Border;
        public PointF[] Points;
        public float Offset;

        readonly uint _gllId;

        public Landscape(uint gllId, float border, params PointF[] points)
        {
            _gllId = gllId;
            Border = border;
            Points = points;

            Compile();
        }

        void Compile()
        {
            gl.NewList(_gllId, gl.COMPILE);

            gl.Begin(gl.LINE_STRIP);

            for (int k = 0; k < Points.Length; k++)
                gl.Vertex3f(Points[k].X, Points[k].Y, 0);

            gl.End();

            gl.EndList();
        }

        public void Draw(float speed)
        {
            gl.PushAttrib(gl.ALL_ATTRIB_BITS);

            Offset += speed;
            if (Offset >= 0f)
                Offset = -50f;

            gl.Color3f(0.0f, 0.1f, 0.0f);
            gl.LineWidth(2f);

            for (int i = -1000; i <= 1000; i += 50)
            {
                gl.PushMatrix();
                gl.Translatef(0f, 0f, Offset + i);
                gl.CallList(_gllId);
                gl.PopMatrix();
            }

            gl.PopAttrib();
        }

        public float GetHeight(float x)
        {
            float h = -10000f;

            for (int i = 0; i < Points.Length; i++)
            {
                if (i < Points.Length - 1)
                {
                    if (Points[i].X < x && Points[i + 1].X > x)
                    {
                        if (Points[i].Y > h)
                            h = Points[i].Y;
                    }
                }
            }

            return h;
        }
    }
}
