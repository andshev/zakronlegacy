using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using ManagedOpenGl;
using MeteorGl.Engine;
using MeteorGl.Engine.Timers;
using MeteorGl.Engine.Utils;

namespace Zakron
{
    public class Program : MetProgram
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Program p = new Program();

            WinStart ws = new WinStart();
            DialogResult dr = ws.ShowDialog();

            if (dr != DialogResult.Yes)
                return;

            MetEngine.FullScreen = ws.cbxFullScreen.Checked;
            MetEngine.Depth = (byte)ws.Devmodes[ws.lbxVideo.SelectedIndex].dmBitsPerPel;
            MetEngine.Width = ws.Devmodes[ws.lbxVideo.SelectedIndex].dmPelsWidth;
            MetEngine.Height = ws.Devmodes[ws.lbxVideo.SelectedIndex].dmPelsHeight;
            MetEngine.LimitFps = ws.cbxOgranFPS.Checked;
            MetEngine.MaxFps = 80;
            MetEngine.StartWindow(p);
        }

        public const uint GLL_MESH_PLAYER = 10;
        public const uint GLL_MESH_ENEMY = 11;
        public const uint GLL_MESH_BOSS = 12;
        public const uint GLL_ROCKETPLAYER = 50;
        public const uint GLL_ROCKETENEMY = 51;
        public const uint GLL_ROCKETBOSS = 52;
        public const uint GLL_LEVEL_DEFAULT = 80;
        public const uint GLL_LEVEL_BOSS = 81;

        bool _paused = false;

        Random _rnd;

        uint _textureFont;
        MetFont _font;

        byte _camMode = 1;
        bool _showFps = false;

        Landscape _levelDefault;
        Landscape _levelBoss;

        Player _player;
        Enemy _enemy;

        readonly List<Sparks> _sparks = new List<Sparks>();

        int _gameoverPrev = 0;

        string comment = "welcome to zakron! destroy enemies to get score and perks!";

        bool _isBossActive;
        Boss _boss;

        public override void Init()
        {
            MetEngine.Window.Text = "Zakron";

            gl.Enable(gl.DEPTH_TEST);
            gl.ClearColor(0f, 0f, 0f, 1f);
            gl.ClearDepth(1f);
            gl.DepthFunc(gl.LEQUAL);
            gl.ShadeModel(gl.SMOOTH);
            gl.Hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST);
            gl.Enable(gl.LINE_SMOOTH);
            gl.Hint(gl.LINE_SMOOTH_HINT, gl.NICEST);
            gl.Enable(gl.BLEND);
            gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

            _textureFont = MetTexture.LoadTexture("../Res/font.bmp");
            _font = new MetFont();
            _font.Load(_textureFont, "../Res/font.txt");

            _rnd = new Random(DateTime.Now.Second + DateTime.Now.Millisecond);

            _levelDefault = new Landscape(
                GLL_LEVEL_DEFAULT, 25f,
                new PointF(-1000f, 10f),
                new PointF(-25, 0f),
                new PointF(-25f, -100f),
                new PointF(25f, -100f),
                new PointF(25f, 0f),
                new PointF(1000, 10f)
            );
            _levelBoss = new Landscape(
                GLL_LEVEL_BOSS, 300f,
                new PointF(-1000f, -170f),
                new PointF(-100f, -110f),
                new PointF(-75f, -155f),
                new PointF(-61f, -107f),
                new PointF(-32f, -123f),
                new PointF(-10f, -101f),
                new PointF(5f, -100f),
                new PointF(25f, -80f),
                new PointF(47f, -97f),
                new PointF(70f, -75f),
                new PointF(92f, -85f),
                new PointF(111f, -70f),
                new PointF(115f, -85f),
                new PointF(150f, -70f),
                new PointF(1000f, -80f)
            );

            _player = new Player("player", Color.Green);
            _enemy = new Enemy("enemy", Color.Violet, _levelDefault);
            _boss = new Boss("boss", Color.Red, _player);
        }

        private void Reset()
        {
            if (_player.Lives < 0)
            {
                _boss.Reset();
                _enemy.Reset(true);
                _player.Reset(true);
            }
            else
                _player.Reset();
        }

        public override void Draw()
        {
            if (_paused)
                return;


            #region FPS counter, clearing and projection

            MetFps.StartFpsCount();

            gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
            MetProjection.Perspective(MetEngine.Width, MetEngine.Height, 45.0, 0.01, 5000.0);
            gl.LoadIdentity();

            if (_camMode == 0)
                glu.LookAt(
                    _player.PosX - _player.Drift, 0f, 0f,
                    _player.PosX - _player.Drift, 0f, -5f,
                    0f, 1f, 0f);
            else if (_camMode == 1)
                glu.LookAt(
                    _player.PosX, 5f, 12f,
                    _player.PosX, 2.5f, 0f,
                    0f, 1f, 0f);
            else if (_camMode == 2)
                glu.LookAt(
                    _player.PosX * 0.98 + _player.Drift, 5f, 15f,
                    _player.PosX * 0.98, 2.5f, 0f,
                    0f, 1f, 0f);
            else if (_camMode == 3)
                glu.LookAt(
                    _player.PosX, 120f, 100f,
                    _player.PosX, 2.5f, -70f,
                    0f, 1f, 0f);
            else if (_camMode == 4)
                glu.LookAt(
                    _player.PosX, 4f, -12f,
                    _player.PosX, 2.5f, 0f,
                    0f, 1f, 0f);

            #endregion


            #region Commentaries

            if (_player.Scores == 0)
            {
                _isBossActive = false;
            }
            else if (_player.Scores >= 1 && _player.Scores < 500)
            {
                comment = "look at that! your first score!";
            }
            else if (_player.Scores >= 500 && _player.Scores < 1000)
            {
                comment = "do not get distracted, continue your flight!";
            }
            else if (_player.Scores >= 1000 && _player.Scores < 2000)
            {
                comment = "your first thousand!";
            }
            else if (_player.Scores >= 2000 && _player.Scores < 3000)
            {
                comment = "the enemies are smart, huh?";
            }
            else if (_player.Scores >= 3000 && _player.Scores < 4000)
            {
                comment = "to the left! to the left! just kidding...";
            }
            else if (_player.Scores >= 4000 && _player.Scores < 5000)
            {
                comment = "ughahahahahahahahahahahahahahaa";
            }
            else if (_player.Scores >= 5000 && _player.Scores < 6000)
            {
                comment = "half way there, pedal to the metal!";
            }
            else if (_player.Scores >= 6000 && _player.Scores < 7000)
            {
                comment = "...do you remember the games on sinclair?..";
            }
            else if (_player.Scores >= 7000 && _player.Scores < 8000)
            {
                comment = "is your controller still working?";
            }
            else if (_player.Scores >= 8000 && _player.Scores < 9000)
            {
                comment = "sensors indicate a big object approaching you";
            }
            else if (_player.Scores >= 9000 && _player.Scores < 10000)
            {
                comment = "boss: i will destroy you!";

                if (!_isBossActive)
                {
                    _isBossActive = true;
                    _boss.Color = Color.Red;
                    _boss.Reset();
                }
            }
            else if (_player.Scores >= 10000 && _player.Scores < 11000)
            {
                _player.FireInterval = 150;
                comment = "your weapon has been upgraded!";
            }
            else if (_player.Scores >= 11000 && _player.Scores < 12000)
            {
                comment = "so what do you think about your new weapon?";
            }
            else if (_player.Scores >= 12000 && _player.Scores < 13000)
            {
                comment = "the game errored out... kidding again!";
            }
            else if (_player.Scores >= 13000 && _player.Scores < 14000)
            {
                comment = "so, do you like it?";
            }
            else if (_player.Scores >= 14000 && _player.Scores < 15000)
            {
                comment = "still cool?";
            }
            else if (_player.Scores >= 15000 && _player.Scores < 16000)
            {
                comment = "wow, 15K scored!";
            }
            else if (_player.Scores >= 16000 && _player.Scores < 17000)
            {
                comment = $"well, max score is still pretty high, - {int.MaxValue}";
            }
            else if (_player.Scores >= 17000 && _player.Scores < 18000)
            {
                comment = "i like this font, hope it is readable";
            }
            else if (_player.Scores >= 18000 && _player.Scores < 19000)
            {
                comment = "cool!";
            }
            else if (_player.Scores >= 19000 && _player.Scores < 20000)
            {
                comment = "boss: the revenge is on its way!!!";

                if (!_isBossActive)
                {
                    _isBossActive = true;
                    _boss.Color = Color.Orange;
                    _boss.Reset();
                }
            }
            else if (_player.Scores >= 20000 && _player.Scores < 21000)
            {
                _player.FireInterval = 200;
                _player.FireLevel = 2;
                comment = "you destroyed another boss!";
            }

            #endregion


            #region Player

            gl.PushMatrix();

            if (_isBossActive)
            {
                if (_player.PosX > _levelBoss.Border)
                    _player.PosX = _levelBoss.Border;
                if (_player.PosX < -_levelBoss.Border)
                    _player.PosX = -_levelBoss.Border;
            }

            if (_player.Speed <= 0.1f)
                _player.Speed = 0.1f;
            if (_player.Speed >= 7f)
                _player.Speed = 7f;

            _player.RotZ /= 1.1f;
            _player.Drift /= 1.1f;

            gl.Translatef(_player.PosX - _player.Drift, _player.PosY, _player.PosZ);
            gl.Rotatef(_player.RotZ, 0f, 0f, 1f);
            _player.DrawStatic();

            gl.PopMatrix();

            #endregion


            #region Rockets, sparks

            foreach (RocketPlayer r in _player.Rockets)
            {
                if (r.Started)
                {
                    gl.PushMatrix();
                    r.Draw();
                    gl.PopMatrix();
                }
            }

            foreach (RocketEnemy r in _enemy.Rockets)
            {
                if (r.Started)
                {
                    gl.PushMatrix();
                    r.Draw();
                    gl.PopMatrix();
                }
            }

            foreach (Sparks spark in _sparks)
            {
                if (spark.Started)
                {
                    gl.PushMatrix();
                    spark.Draw();
                    gl.PopMatrix();
                }
            }

            if (_isBossActive)
            {
                foreach (RocketBoss r in _boss.Rockets)
                {
                    if (r.Started)
                    {
                        gl.PushMatrix();
                        r.Draw();
                        gl.PopMatrix();
                    }
                }
            }

            #endregion


            #region Enemies

            if (!_isBossActive)
            {
                gl.PushMatrix();

                _enemy.NavigateFire();

                _enemy.RotZ /= 1.1f;
                _enemy.Drift /= 1.1f;

                gl.Translatef(_enemy.PosX - _enemy.Drift, _enemy.PosY, _enemy.PosZ);
                gl.Rotatef(180f, 0f, 1f, 0f);
                gl.Rotatef(_enemy.RotZ, 0f, 0f, -1f);

                if (_enemy.PosX + _enemy.SizeW / 2 >= _levelDefault.Border || _enemy.PosX - _enemy.SizeW / 2 <= -_levelDefault.Border)
                    _enemy.Destroy();

                if (_enemy.Destroyed)
                {
                    _enemy.DSF -= 0.02f;
                    _enemy.PosZ -= 1f;

                    if (_enemy.DSF <= 0)
                    {
                        _enemy.Reset(true);
                    }
                    else
                    {
                        _enemy.Draw();
                    }
                }
                else
                    _enemy.Draw();

                if (_enemy.PosZ > 20f)
                    _enemy.Reset(true);

                gl.PopMatrix();
            }

            #endregion


            #region Bosses

            if (_isBossActive)
            {
                gl.PushMatrix();

                if (!_boss.Destroyed())
                {
                    if (_boss.CanDamage)
                        _boss.ThinkNFire();
                    _boss.Draw();
                }
                else
                {
                    Score(1001);
                    _player.PosX = 0f;
                    _isBossActive = false;
                }

                gl.PopMatrix();
            }

            #endregion


            #region Collision detection

            if (_player.Destroyed)
            {
                _player.DSF -= 0.01f;

                if (_player.DSF <= 0.0f)
                    Reset();
            }

            if (!_isBossActive)
            {
                // Collisions with enemies
                if (!_enemy.Destroyed && !_player.Destroyed)
                {
                    if (_player.CollidesWith(_enemy))
                    {
                        _enemy.Destroy();
                        _player.Destroy();
                    }
                }


                // Collisions with landscape
                if (Math.Abs(_player.PosX) > _levelDefault.Border - _player.SizeW / 2)
                {
                    if (!_player.Destroyed)
                        _player.Destroy();
                }


                // Collisions with rockets
                if (!_player.Destroyed)
                {
                    foreach (RocketEnemy rt in _enemy.Rockets)
                    {
                        if (rt.Started)
                        {
                            if (rt.CollidesWith(_player))
                            {
                                rt.DoBoom();
                                Sparks s = new Sparks(_player.PosX, _player.PosY, _player.PosZ, _player);
                                _sparks.Add(s);
                                s.Color = _enemy.Color;
                                s.Start();
                                _player.Destroy();
                            }
                        }
                    }
                }
            }
            else
            {
                if (!_player.Destroyed)
                {
                    foreach (RocketBoss rb in _boss.Rockets)
                    {
                        if (rb.Started)
                        {
                            if (rb.CollidesWith(_player))
                            {
                                rb.DoBoom();
                                Sparks s = new Sparks(_player.PosX, _player.PosY, _player.PosZ, _player);
                                _sparks.Add(s);
                                s.Color = _boss.Color;
                                s.Start();
                                _player.Destroy();
                            }
                        }
                    }
                }
            }


            // Collision detection for player rockets and enemies
            if (!_player.Destroyed)
            {
                foreach (RocketPlayer rs in _player.Rockets)
                {
                    if (rs.Started)
                    {
                        if (!_isBossActive)
                        {
                            if (!_enemy.Destroyed)
                            {
                                if (rs.CollidesWith(_enemy))
                                {
                                    rs.DoBoom();
                                    Sparks s = new Sparks(_enemy.PosX, _enemy.PosY, _enemy.PosZ, _player);
                                    _sparks.Add(s);
                                    s.Color = _enemy.Color;
                                    s.Start();
                                    _enemy.Destroy();
                                    Score();
                                }
                            }
                        }
                        else
                        {
                            if (_boss.CanDamage)
                            {
                                for (int bt = 0; bt < _boss.Turrels.Count; bt++)
                                {
                                    if (rs.CollidesWith(_boss.Turrels[bt]))
                                    {
                                        _boss.Turrels[bt].Damage();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            #endregion


            #region Landscape

            gl.PushMatrix();

            if (_isBossActive)
                _levelBoss.Draw(_player.Speed);
            else
                _levelDefault.Draw(_player.Speed);

            gl.PopMatrix();

            #endregion


            #region UI

            gl.PushAttrib(gl.ALL_ATTRIB_BITS);
            MetProjection.Ortho(MetEngine.Width, MetEngine.Height, 0, 800, 600, 0, 0.1, 10.0);
            gl.LoadIdentity();

            gl.PushMatrix();

            gl.Translatef(5f, 30f, -5f);
            gl.Color4f(1f, 1f, 0f, 1f);
            _font.Print($"score: {_player.Scores}", 30f);

            gl.Translatef(520f, 0f, 0f);
            gl.Color4f(.7f, .7f, .1f, 1f);
            _font.Print($"lives: {_player.Lives}", 20f);

            gl.Translatef(-475f, -20f, 0f);
            gl.Color4f(.7f, .7f, .7f, .9f);
            _font.Print(comment, 10f);

            gl.PopMatrix();

            if (_showFps)
            {
                gl.PushMatrix();
                gl.Translatef(700f, 10f, -5f);
                gl.Color4f(0.5f, 0.5f, 1f, 1f);
                _font.Print($"FPS = {MetFps.Fps}", 10f);
                gl.PopMatrix();
            }

            if (!_player.Destroyed)
            {
                if (!_isBossActive)
                {
                    gl.PushMatrix();
                    
                    gl.Translatef(5f, 523f, -5f);
                    gl.Color4f(Math.Abs(_player.PosX) / _levelDefault.Border, .2f, .2f, .9f);
                    _font.Print((_levelDefault.Border - _player.SizeW / 2 - Math.Abs(_player.PosX)).ToString(), 20f);

                    gl.Translatef(85f, 7f, 0f);
                    gl.Color4f(.5f, .5f, .5f, .5f);
                    _font.Print("ft till landscape", 10f);

                    gl.Translatef(315f, 0f, 0f);
                    gl.Color4f(.5f, .5f, .5f, .5f);
                    _font.Print($"enemy rockets = {_enemy.Rockets.Count}", 10f);
                    
                    gl.PopMatrix();
                }
                else
                {
                    gl.PushMatrix();
                    gl.Color4f(.5f, .5f, .5f, .5f);

                    gl.Translatef(5f, 530f, -5f);
                    _font.Print($"turrets: {_boss.Turrels[0].Health} {_boss.Turrels[1].Health} {_boss.Turrels[2].Health} {_boss.Turrels[3].Health} {_boss.Turrels[4].Health} {_boss.Turrels[5].Health} {_boss.Turrels[6].Health} {_boss.Turrels[7].Health}", 10f);

                    gl.PopMatrix();
                }

                gl.PushMatrix();

                gl.Translatef(5f, 550f, -5f);
                gl.Color4f(0.5f, 0.5f, 0.5f, 0.3f);
                gl.Begin(gl.QUADS);
                gl.Vertex2f(0f, 0f);
                gl.Vertex2f(_player.Speed * 100f, 0f);
                gl.Vertex2f(_player.Speed * 100f, 20f);
                gl.Vertex2f(0f, 20f);
                gl.End();
                gl.Translatef(0f, 5f, 0f);
                gl.Color4f(.8f, .8f, .8f, .8f);
                _font.Print($"speed = {_player.Speed}", 10f);
                
                gl.PopMatrix();
            }
            else
            {
                gl.PushMatrix();
                gl.Translatef(250f, 530f, -5f);
                gl.Color4f(.5f, .5f, .5f, .5f);
                _font.Print("destroyed", 20f);
                gl.PopMatrix();

                _gameoverPrev = MetTimer.Count();
            }

            gl.PopAttrib();

            #endregion


            #region Clean up

            for (int i = 0; i < _player.Rockets.Count; i++)
            {
                if (_player.Rockets[i].FlewAvay)
                    _player.Rockets.RemoveAt(i);
            }

            for (int i = 0; i < _enemy.Rockets.Count; i++)
            {
                if (_enemy.Rockets[i].FlewAway)
                    _enemy.Rockets.RemoveAt(i);
            }

            for (int i = 0; i < _boss.Rockets.Count; i++)
            {
                if (_boss.Rockets[i].FlewAway)
                    _boss.Rockets.RemoveAt(i);
            }

            for (int i = 0; i < _sparks.Count; i++)
            {
                if (_sparks[i].Maximum)
                    _sparks.RemoveAt(i);
            }

            #endregion
        }
	    
        private void Score()
        {
            int s = (int)((800 + _enemy.PosZ / 10) * _player.Speed / 10);

            if (s >= 1000)
                s = 888;

            _player.Scores += s;
        }

        private void Score(int s)
        {
            _player.Scores += s;
        }

        public override void Input()
        {
            if (MetInput.KeyPressed(Keys.Escape))
                MetEngine.Stop();
            if (MetInput.KeyPressed(Keys.P))
                _paused = !_paused;

            if (_paused)
                return;

            if (MetInput.KeyPressed(Keys.F1))
                _camMode = 0;
            if (MetInput.KeyPressed(Keys.F2))
                _camMode = 1;
            if (MetInput.KeyPressed(Keys.F3))
                _camMode = 2;
            if (MetInput.KeyPressed(Keys.F4))
                _camMode = 3;
            if (MetInput.KeyPressed(Keys.F5))
                _camMode = 4;
            if (MetInput.KeyPressed(Keys.F12))
                _showFps = !_showFps;
            if (MetInput.KeyHold(Keys.Left))
            {
                _player.RotZ += 3f;
                _player.PosX -= 1.0f;
                _player.Drift -= 0.2f;
            }
            if (MetInput.KeyHold(Keys.Right))
            {
                _player.RotZ -= 3f;
                _player.PosX += 1.0f;
                _player.Drift += 0.2f;
            }
            if (MetInput.KeyHold(Keys.Up))
                _player.Speed += 0.2f;
            if (MetInput.KeyHold(Keys.Down))
                _player.Speed -= 0.2f;
            if (MetInput.KeyHold(Keys.Space))
                _player.Fire();
        }
    }
}
