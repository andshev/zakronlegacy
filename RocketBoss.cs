﻿using ManagedOpenGl;
using MeteorGl.Engine.Timers;
using MeteorGl.Mathematics;

namespace Zakron
{
    public class RocketBoss : ZakronObject
    {
        public bool Started;
        public bool FlewAway;
        public bool Boom;
        public Vector3 StartPoint;
        public Vector3 Target;

        readonly Boss _boss;
        int _timeStop;

        public RocketBoss(Boss bb, Vector3 startpoint, Vector3 target)
        {
            _boss = bb;
            StartPoint = startpoint;
            RadiusZ = 2.0f;
            Target = new Vector3(
                target.X - startpoint.X,
                target.Y - startpoint.Y,
                target.Z - startpoint.Z
            );
            Target.Normalize();
            PosX = StartPoint.X;
            PosY = StartPoint.Y;
            PosZ = StartPoint.Z;
            Started = false;
            Compile();
        }

        public void Start()
        {
            _timeStop = MetTimer.Count() + 10000;
            Started = true;
        }

        public void DoBoom()
        {
            Boom = true;
            Started = false;
        }

        public void FlyAway()
        {
            Started = false;
            FlewAway = true;
        }

        public void Compile()
        {
            gl.NewList(Program.GLL_ROCKETBOSS, gl.COMPILE);
            gl.PushAttrib(gl.ALL_ATTRIB_BITS);

            gl.LineWidth(2f);
            gl.Color3ub(_boss.Color.R, _boss.Color.G, _boss.Color.B);
            gl.Begin(gl.LINES);
            gl.Vertex3f(-1f, 0f, 0f);
            gl.Vertex3f(1f, 0f, 0f);
            gl.Vertex3f(0f, 1f, 0f);
            gl.Vertex3f(0f, -1f, 0f);
            gl.Vertex3f(0f, 0f, 1f);
            gl.Vertex3f(0f, 0f, -1f);
            gl.End();

            gl.PopAttrib();
            gl.EndList();
        }

        public void Draw()
        {
            if (MetTimer.Count() >= _timeStop)
            {
                FlyAway();
                return;
            }

            if (PosZ >= 5f)
            {
                FlyAway();
                return;
            }

            if (PosX >= _boss.SizeW)
            {
                FlyAway();
                return;
            }

            if (PosX <= -_boss.SizeW)
            {
                FlyAway();
                return;
            }

            PosX += Target.X;
            PosY += Target.Y;
            PosZ += Target.Z;

            gl.Translatef(PosX, PosY, PosZ);
            //gl.Rotatef(-35f, 1f, 0f, 0f);
            gl.CallList(Program.GLL_ROCKETBOSS);
        }
    }
}
