﻿using System;
using System.Drawing;
using ManagedOpenGl;
using MeteorGl.Engine.Utils;

namespace Zakron
{
    public class Sparks : ZakronObject
    {
        public const int MAX_SPARKS = 128;

        public struct Particle
        {
            public bool Active;
            public float Life;
            public float Fade;
            public float R;
            public float G;
            public float B;
            public float X;
            public float Y;
            public float Z;
            public float Xi;
            public float Yi;
            public float Zi;
            public float Xg;
            public float Yg;
            public float Zg;
        }

        public float Fade = 1f;
        public float DSF = 1f;
        public Particle[] Points = new Particle[MAX_SPARKS];
        public bool Started;
        public bool Maximum;

        readonly uint _texture;

        readonly Random _rnd;
        readonly Player _player;

        public Sparks(float x, float y, float z, Player samolet)
        {
            _player = samolet;
            PosX = x;
            PosY = y;
            PosZ = z;
            _texture = MetTexture.LoadTexture(@"..\Res\spark.bmp");
            Color = Color.Yellow;
            _rnd = new Random(DateTime.Now.Millisecond);
        }

        public void Start()
        {
            Started = false;
            DSF = 1f;

            for (int i = 0; i < Points.Length; i++)
            {
                Points[i].Active = true;
                Points[i].Life = 1f;
                Points[i].R = Color.R / 255f + 0.3f;
                Points[i].G = Color.G / 255f + 0.3f;
                Points[i].B = Color.B / 255f + 0.3f;
                Points[i].Xi = _rnd.Next(-30, 30) * 20f;
                Points[i].Yi = _rnd.Next(-30, 30) * 20f;
                Points[i].Zi = _rnd.Next(-30, 30) * 20f;
                Points[i].Xg = 0f;
                Points[i].Yg = -9.8212f;
                Points[i].Zg = 0f;
            }

            Started = true;
        }

        public void Draw()
        {
            if (Started)
            {
                gl.PushAttrib(gl.ALL_ATTRIB_BITS);

                gl.Enable(gl.TEXTURE_2D);
                gl.Enable(gl.BLEND);
                gl.BlendFunc(gl.SRC_ALPHA, gl.ONE);
                gl.DepthMask(false);
                gl.Hint(gl.POINT_SMOOTH_HINT, gl.NICEST);

                gl.PointSize(1f);
                gl.BindTexture(gl.TEXTURE_2D, _texture);

                gl.Translatef(PosX, PosY, PosZ);

                for (int i = 0; i < MAX_SPARKS; i++)
                {
                    if (Points[i].Active)
                    {
                        float x = Points[i].X;
                        float y = Points[i].Y;
                        float z = Points[i].Z;

                        gl.Color4f(Points[i].R, Points[i].G, Points[i].B, DSF);

                        gl.Begin(gl.TRIANGLE_STRIP);

                            gl.TexCoord2f(1f, 1f);
                            gl.Vertex3f(x + 0.5f, y + 0.5f, z);

                            gl.TexCoord2f(0f, 1f);
                            gl.Vertex3f(x - 0.5f, y + 0.5f, z);

                            gl.TexCoord2f(1f, 0f);
                            gl.Vertex3f(x + 0.5f, y - 0.5f, z);

                            gl.TexCoord2f(0f, 0f);
                            gl.Vertex3f(x - 0.5f, y - 0.5f, z);

                        gl.End();

                        Points[i].X += Points[i].Xi / (Fade * 1000);
                        Points[i].Y += Points[i].Yi / (Fade * 1000);
                        Points[i].Z += Points[i].Zi / (Fade * 1000);
                        Points[i].Xi += Points[i].Xg / 5f;
                        Points[i].Yi += Points[i].Yg / 2f;
                        Points[i].Zi += Points[i].Zg / 5f;
                        Points[i].Life -= Points[i].Fade;

                        if (Points[i].Life < 0f)
                        {
                            Points[i].Life = 1.0f;
                            Points[i].Fade = _rnd.Next(100) / 1000.0f + 0.003f;
                            Points[i].X = 0.0f;
                            Points[i].Y = 0.0f;
                            Points[i].Z = 0.0f;
                            Points[i].Xi = _rnd.Next(-50, 50);
                            Points[i].Yi = _rnd.Next(-50, 50);
                            Points[i].Zi = _rnd.Next(-50, 50);
                        }
                    }
                }

                gl.PopAttrib();

                PosZ += _player.Speed - 1f;

                DSF -= 0.005f;

                if (DSF <= 0f)
                    Maximum = true;
            }
        }
    }
}
