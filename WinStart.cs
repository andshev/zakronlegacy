﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MeteorGl.Engine.Utils;

namespace Zakron
{
    public partial class WinStart : Form
    {
        public readonly List<MetScreen.DeviceSettings> Devmodes = MetScreen.GetAllSettings();

        public WinStart()
        {
            InitializeComponent();
        }

        private void WinStart_Load(object sender, EventArgs e)
        {
            foreach (MetScreen.DeviceSettings dm in Devmodes)
            {
                lbxVideo.Items.Add($"{dm.dmPelsWidth}x{dm.dmPelsHeight} {dm.dmBitsPerPel}bit {dm.dmDisplayFrequency}hz");
                
                if (lbxVideo.Items.Count > 0)
                {
                    if (dm.dmPelsWidth == 800 && dm.dmPelsHeight == 600 && dm.dmBitsPerPel == 32)
                        lbxVideo.SelectedIndex = lbxVideo.Items.Count - 1;
                }
            }

            if (lbxVideo.SelectedIndex < 0)
                lbxVideo.SelectedIndex = 0;
        }
    }
}
